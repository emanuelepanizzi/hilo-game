package api

import (
	"net/http"
)

// Handler returns an instance of httprouter.Router that handle APIs registered here
func (rt *_router) Handler() http.Handler {
	// Register routes
	rt.router.GET("/games", rt.listAllGames)
	rt.router.POST("/games", rt.startNewGame)
	
	rt.router.POST("/games/:id", rt.makeAGuess)
	rt.router.GET("/games/:id", rt.listGuesses)
	
	rt.router.GET("/", rt.getHelloWorld)
	rt.router.GET("/context", rt.wrap(rt.getContextReply))

	// Special routes
	rt.router.GET("/liveness", rt.liveness)

	return rt.router
}
